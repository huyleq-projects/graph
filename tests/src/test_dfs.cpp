#include <iostream>
#include <fstream>
#include <tuple>

#include "boost/graph/graphviz.hpp"

#include "graph.h"

int main(int argc, char **argv){
    boost_digraph g;

    boost_vertex_descriptor v0 = boost::add_vertex(0, g);
    boost_vertex_descriptor v1 = boost::add_vertex(1, g);
    boost_vertex_descriptor v2 = boost::add_vertex(2, g);
    boost_vertex_descriptor v3 = boost::add_vertex(3, g);
    boost_vertex_descriptor v4 = boost::add_vertex(4, g);
    boost_vertex_descriptor v5 = boost::add_vertex(5, g);

    boost::add_edge(v0, v1, 0, g);
    boost::add_edge(v0, v2, 0, g);
    boost::add_edge(v1, v3, 0, g);
    boost::add_edge(v2, v1, 0, g);
    boost::add_edge(v3, v2, 0, g);
    boost::add_edge(v4, v3, 0, g);
    boost::add_edge(v4, v5, 0, g);
    
    VertexWriter<boost_digraph> vertex_writer(g);
	EdgeWriter<boost_digraph> edge_writer(g);
    
    std::ofstream f("graph.dot");
    boost::write_graphviz(f, g, vertex_writer, edge_writer);

    dfs(g);
    
    boost_vertex_iterator it, it_end;
    for(std::tie(it, it_end) = boost::vertices(g); it != it_end; it++)
        std::cout << *it << " " << g[*it] << std::endl;

    return 0;
}
