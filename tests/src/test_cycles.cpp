#include <iostream>
#include <fstream>
#include <tuple>

#include "boost/graph/graphviz.hpp"

#include "graph.h"

int main(int argc, char **argv){
    boost_digraph g;
    std::unordered_map<int, boost_vertex_descriptor> index_to_vertex_map;

    boost_vertex_descriptor v0 = boost::add_vertex(0, g); index_to_vertex_map[0] = v0;
    boost_vertex_descriptor v1 = boost::add_vertex(1, g); index_to_vertex_map[1] = v1;
    boost_vertex_descriptor v2 = boost::add_vertex(2, g); index_to_vertex_map[2] = v2;
    boost_vertex_descriptor v3 = boost::add_vertex(3, g); index_to_vertex_map[3] = v3;
    boost_vertex_descriptor v4 = boost::add_vertex(4, g); index_to_vertex_map[4] = v4;

    boost::add_edge(v0, v1, 0, g);
    boost::add_edge(v0, v4, 0, g);
    boost::add_edge(v1, v2, 0, g);
    boost::add_edge(v2, v3, 0, g);
    boost::add_edge(v3, v0, 0, g);
    boost::add_edge(v4, v3, 0, g);
    
    VertexWriter<boost_digraph> vertex_writer(g);
	EdgeWriter<boost_digraph> edge_writer(g);
    
    std::ofstream f("graph.dot");
    boost::write_graphviz(f, g, vertex_writer, edge_writer);
    std::vector<std::vector<int>> cycles;

    int ncycles = find_cycles(g, cycles);
    std::cout << "found: " << ncycles << std::endl;
    print_cycles(cycles);

    return 0;
}
