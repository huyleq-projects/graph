#include <iostream>
#include <fstream>
#include <tuple>
#include <utility>

#include <boost/graph/strong_components.hpp>
#include "boost/graph/graphviz.hpp"

#include "graph.h"

int main(int argc, char **argv){
    boost_digraph g;

    boost_vertex_descriptor v1 = boost::add_vertex({1, Color::grey,  0, 0, -1}, g);
    boost_vertex_descriptor v0 = boost::add_vertex({0, Color::white, 0, 0, -1}, g);
    if(v1 < v0) std::cout << "v1 < v0" << std::endl;
    else std::cout << "v1 >= v0" << std::endl;
    boost_vertex_descriptor v3 = boost::add_vertex({3, Color::white, 0, 0, -1}, g);
    boost_vertex_descriptor v5 = boost::add_vertex({5, Color::white, 0, 0, -1}, g);
    boost_vertex_descriptor v4 = boost::add_vertex({4, Color::white, 0, 0, -1}, g);
    boost_vertex_descriptor v6 = boost::add_vertex({6, Color::white, 0, 0, -1}, g);
    boost_vertex_descriptor v2 = boost::add_vertex({2, Color::black, 0, 0, -1}, g);

    std::pair<boost_edge_descriptor, bool> edge_pair = boost::add_edge(v0, v1, 0, g);
    std::cout << g[edge_pair.first] << std::endl;
    boost::add_edge(v6, v1, 0, g);
    boost::add_edge(v1, v0, 0, g);
    boost::add_edge(v2, v4, 0, g);
    boost::add_edge(v3, v5, 0, g);
    boost::add_edge(v3, v2, 0, g);
    boost::add_edge(v5, v4, 0, g);
    boost::add_edge(v4, v3, 0, g);
   
    boost_digraph g0 = g;
    print_graph(g0);
    
    std::vector<int> component_ids(boost::num_vertices(g0));
    int num_components = boost::strong_components(g0, component_ids.data());
    std::cout << "found " << num_components << " strongly connected components" << std::endl;
    for(int i = 0; i < boost::num_vertices(g0); i++)
        std::cout << "i: " << i << " component: " << component_ids[i] << std::endl;

    std::ofstream f0("graph.dot");
    boost_vertex_writer vertex_writer0(g0);
	boost_edge_writer edge_writer0(g0);
    boost::write_graphviz(f0, g0, vertex_writer0, edge_writer0);
    
    std::vector<boost_digraph> components;
    nontrivial_strong_components(g0, components);
    num_components = components.size();
    std::cout << "found " << num_components << " strongly connected nontrivial components" << std::endl;
    for(int i = 0; i < num_components; i++){
        std::cout << "component: " << i << std::endl;
        print_graph(components[i]);
        boost_vertex_writer vertex_writer(components[i]);
        boost_edge_writer edge_writer(components[i]);
        std::ofstream f("component_" + std::to_string(i) + ".dot");
        boost::write_graphviz(f, components[i], vertex_writer, edge_writer);
    }
    std::cout << std::endl;

    boost_vertex_iterator it = boost::vertices(g).first;
    boost::clear_vertex(*it, g0);
    boost::remove_vertex(*it, g0);
    print_graph(g0);

    return 0;
}
