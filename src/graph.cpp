#include <algorithm>
#include <stack>
#include <unordered_set>

#include "graph.h"
#include <boost/graph/strong_components.hpp>

template class VertexWriter<boost_digraph>;
template class EdgeWriter  <boost_digraph>;

void print_graph(const boost_digraph &g){
    boost_vertex_iterator it, it_end;
    for(std::tie(it, it_end) = boost::vertices(g); it != it_end; it++){
        std::cout << *it << " " << g[*it] << " neighbor:";
        boost_adjacency_iterator i, e;;
        for(std::tie(i, e) = boost::adjacent_vertices(*it, g); i != e; i++) 
            std::cout << " " << g[*i].index;
        std::cout << std::endl;
    }
    return;
}

void dfs(boost_digraph &g, std::vector<boost_vertex_descriptor> *topo_order){
    boost_vertex_iterator it, it_end;
    for(std::tie(it, it_end) = boost::vertices(g); it != it_end; it++){
        g[*it].color = Color::white;
        g[*it].parent = -1;
    }

    float time = 0;

    for(std::tie(it, it_end) = boost::vertices(g); it != it_end; it++){
        if(g[*it].color == Color::white) dfs_visit(g, *it, time, topo_order);
    }

    if(topo_order) std::reverse(topo_order->begin(), topo_order->end());
    return;
}

void dfs_visit(boost_digraph &g,
               const boost_vertex_descriptor desc,
               float &time,
               std::vector<boost_vertex_descriptor> *topo_order){
    g[desc].discover_time = (++time);
    g[desc].color = Color::grey;
    
    boost_adjacency_iterator i, e;;
    for(std::tie(i, e) = boost::adjacent_vertices(desc, g); i != e; i++) {
        if(g[*i].color == Color::white){
            g[*i].parent = g[desc].index;
            dfs_visit(g, *i, time, topo_order);
        }
    }
    
    g[desc].finish_time = (++time);
    g[desc].color = Color::black;
    if(topo_order) topo_order->push_back(desc);
    return;
}

void subgraph(const boost_digraph &g,
              const std::vector<boost_vertex_descriptor> &subset,
              boost_digraph &subg){
    std::unordered_map<boost_vertex_descriptor, boost_vertex_descriptor> vertex_map;
    int n = subset.size(), n1 = n-1;
    
    for(int i = 0; i < n; i++){
        boost_vertex_descriptor g_vertex = subset[i];
        boost_vertex_descriptor subg_vertex = boost::add_vertex(VertexProperty(g[g_vertex]), subg);
        vertex_map[g_vertex] = subg_vertex;
    }
    
    for(int i = 0; i < n1; i++){
        boost_vertex_descriptor vi = subset[i];
        boost_vertex_descriptor ti = vertex_map[vi];
        for(int j = i+1; j < n; j++){
            boost_vertex_descriptor vj = subset[j];
            boost_vertex_descriptor tj = vertex_map[vj];
            if(boost::edge(vi, vj, g).second) boost::add_edge(ti, tj, subg);
            if(boost::edge(vj, vi, g).second) boost::add_edge(tj, ti, subg);
        }
    }
    return;
}

int nontrivial_strong_components(const boost_digraph &g, std::vector<boost_digraph> &components){
    unsigned nv = boost::num_vertices(g);
    std::vector<int> component_ids(nv);
    int num_components = boost::strong_components(g, component_ids.data());

    std::vector<std::vector<boost_vertex_descriptor>> subsets(num_components);
    for(unsigned i = 0; i < nv; i++) subsets[component_ids[i]].push_back(boost_vertex_descriptor(i));

    int count = 0;
    for(int i = 0; i < num_components; i++){
        if(subsets[i].size() > 1) count++;
    }
    
    components.resize(count); count = 0;
    for(const std::vector<boost_vertex_descriptor> &subset: subsets){
        if(subset.size() > 1) subgraph(g, subset, components[count++]);
    }
    return num_components;
}

// implementation of Johnson's algorithm for finding all elementary cycles
// Johnson, 1975, Finding all the elementary circuits of a directed graph
// SIAM Journal of Computing, Vol. 4.

void unblock(boost_vertex_descriptor u,
             std::vector<bool> &blocked,
             std::unordered_map<boost_vertex_descriptor,
             std::unordered_set<boost_vertex_descriptor>> &bmap){
    blocked[u] = false;
    std::unordered_set<boost_vertex_descriptor> &bmap_u = bmap[u];
    for(boost_vertex_descriptor w: bmap_u){
        if(blocked[w]) unblock(w, blocked, bmap);
    }
    bmap_u.clear();
    return;
}

bool find_cycles_scc_start_helper(boost_vertex_descriptor s,
                                  boost_vertex_descriptor v,
                                  std::stack<boost_vertex_descriptor> &vstack,
                                  std::vector<bool> &blocked,
                                  std::unordered_map<boost_vertex_descriptor,
                                  std::unordered_set<boost_vertex_descriptor>> &bmap,
                                  std::vector<std::vector<int>> &cycles,
                                  const boost_digraph &g){
    bool found = false;
    vstack.push(v); blocked[v] = true;

    boost_adjacency_iterator i, e;
    for(std::tie(i, e) = boost::adjacent_vertices(v, g); i != e; i++){
        boost_vertex_descriptor w = (*i);
        if(w == s){
            found = true;
            cycles.resize(cycles.size()+1);
            std::stack<boost_vertex_descriptor> tmp(vstack);
            while(!tmp.empty()){
                cycles.back().push_back(g[tmp.top()].index);
                tmp.pop();
            }
            std::reverse(cycles.back().begin(), cycles.back().end());
            cycles.back().push_back(g[s].index);
        }
        else if(!blocked[w])
            found = find_cycles_scc_start_helper(s, w, vstack, blocked, bmap, cycles, g);
    }
    
    if(found) unblock(v, blocked, bmap);
    else{
        for(std::tie(i, e) = boost::adjacent_vertices(v, g); i != e; i++){
            boost_vertex_descriptor w = (*i);
            if(bmap.find(w) == bmap.end()) 
                bmap[w] = std::unordered_set<boost_vertex_descriptor>({v});
            else if(bmap[w].find(v) == bmap[w].end()) bmap[w].insert(v);
        }

    }
    vstack.pop();

    return found;
}

void find_cycles_scc_start(boost_vertex_descriptor s,
                           boost_digraph &g,
                           std::vector<std::vector<int>> &cycles){
    int nv = boost::num_vertices(g);
    std::vector<bool> blocked(nv);
    for(int i = 0; i < nv; i++) blocked[i] = false;
    std::stack<boost_vertex_descriptor> vstack;
    std::unordered_map<boost_vertex_descriptor,
    std::unordered_set<boost_vertex_descriptor>> bmap;
    find_cycles_scc_start_helper(s, s, vstack, blocked, bmap, cycles, g);
    return;
}

void find_cycles_scc(boost_digraph &g, std::vector<std::vector<int>> &cycles){
    boost_vertex_descriptor s = *(boost::vertices(g).first);
    find_cycles_scc_start(s, g, cycles);
    boost::clear_vertex(s, g);
    boost::remove_vertex(s, g);
    find_cycles(g, cycles);
    return;
}

int find_cycles(const boost_digraph &g, std::vector<std::vector<int>> &cycles){
    std::vector<boost_digraph> components;
    nontrivial_strong_components(g, components);
    if(!components.empty()){
        for(boost_digraph &component: components){
            if(boost::num_vertices(component) == 2){
                cycles.resize(cycles.size()+1);
                boost_vertex_iterator it, it_end;
                for(std::tie(it, it_end) = boost::vertices(component); it != it_end; it++)
                    cycles.back().push_back(component[*it].index);
            }
            else find_cycles_scc(component, cycles);
        }
    }
    return cycles.size();
}

void print_cycles(const std::vector<std::vector<int>> &cycles){
    for(const std::vector<int> &cycle: cycles){
        int n = cycle.size();
        std::cout << cycle[0];
        for(int i = 1; i < n; i++) std::cout << " -> " << cycle[i];
        std::cout << std::endl;
    }
    return;
}
