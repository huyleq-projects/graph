#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <utility>
#include <vector>
#include <unordered_map>

#include "boost/graph/adjacency_list.hpp"
#include "boost/graph/graph_traits.hpp"

enum class Color{ white, grey, black};

static std::ostream & operator<<(std::ostream &os, Color c){
    switch(c){
        case Color::white :return os << "white";
        case Color::grey  :return os << "grey";
        case Color::black :return os << "black";
    }
}

struct VertexProperty{
    VertexProperty(): index(0),
                      color(Color::white),
                      discover_time(0),
                      finish_time(0),
                      parent(-1){}

    VertexProperty(int i): index(i),
                           color(Color::white),
                           discover_time(0),
                           finish_time(0),
                           parent(-1){}

    VertexProperty(int i, Color c, float dt, float ft, int p): index(i),
                                                               color(c),
                                                               discover_time(dt),
                                                               finish_time(ft),
                                                               parent(p){}

    VertexProperty(const VertexProperty &other): index(other.index),
                                                 color(other.color),
                                                 discover_time(other.discover_time),
                                                 finish_time(other.finish_time),
                                                 parent(other.parent){}

    friend std::ostream & operator<<(std::ostream &os, const VertexProperty &v){
        return os << "index: "           << v.index
                  << ", color: "         << v.color
                  << ", discover_time: " << v.discover_time
                  << ", finish_time: "   << v.finish_time 
                  << ", parent: "        << v.parent;
    }

    int   index;
    Color color;
    float discover_time;
    float finish_time;
    int   parent;
};

struct EdgeProperty{
    EdgeProperty(): weight(0){}
    EdgeProperty(float w): weight(w){}

    friend std::ostream & operator<<(std::ostream &os, const EdgeProperty &e){
        return os << "weight: " << e.weight;
    }

    float weight;
};

template <class Graph>
class VertexWriter{
    public:
    VertexWriter(Graph _graph) : graph(_graph){}
    template <class Vertex> void operator()(std::ostream &out, const Vertex &v) const{
	    VertexProperty prop = graph[v];
		std::string fillcolor, fontcolor;
		switch(prop.color){
		    case Color::white: fillcolor = "white"; fontcolor = "black"; break;
		    case Color::grey : fillcolor = "grey";  fontcolor = "black"; break;
		    case Color::black: fillcolor = "black"; fontcolor = "white"; break;
		}
		out << "[label=\"" << prop.index 
            << "\" fillcolor=\"" << fillcolor
            << "\" fontcolor=\"" << fontcolor 
            << "\" style=filled]";
    }
    
    private:
    Graph graph;
};

template <class Graph>
class EdgeWriter{
    public:
    EdgeWriter(Graph _graph) : graph(_graph) {}
    template <class Edge>
    void operator()(std::ostream &out, const Edge &e) const{
	    EdgeProperty prop = graph[e];
		out << "[label=\"" << prop.weight << "\"]";
    }

    private:
    Graph graph;
};

typedef boost::adjacency_list<boost::vecS,
                              boost::vecS,
                              boost::directedS,
                              VertexProperty, 
                              EdgeProperty> boost_digraph;

typedef boost::graph_traits<boost_digraph>::vertex_descriptor  boost_vertex_descriptor;
typedef boost::graph_traits<boost_digraph>::edge_descriptor    boost_edge_descriptor;

typedef boost::graph_traits<boost_digraph>::vertex_iterator    boost_vertex_iterator;
typedef boost::graph_traits<boost_digraph>::edge_iterator      boost_edge_iterator;
typedef boost::graph_traits<boost_digraph>::out_edge_iterator  boost_out_edge_iterator;
typedef boost::graph_traits<boost_digraph>::in_edge_iterator   boost_in_edge_iterator;
typedef boost::graph_traits<boost_digraph>::adjacency_iterator boost_adjacency_iterator;

typedef VertexWriter<boost_digraph> boost_vertex_writer;
typedef EdgeWriter  <boost_digraph> boost_edge_writer;

void print_graph(const boost_digraph &g);
void dfs(boost_digraph &g,
         std::vector<boost_vertex_descriptor> *topo_order = nullptr);
void dfs_visit(boost_digraph &g,
               const boost_vertex_descriptor desc,
               float &time,
               std::vector<boost_vertex_descriptor> *topo_order = nullptr);
void subgraph(const boost_digraph &g,
              const std::vector<boost_vertex_descriptor> &subset,
              boost_digraph &subg);
int nontrivial_strong_components(const boost_digraph &g, std::vector<boost_digraph> &components);
int find_cycles(const boost_digraph &g, std::vector<std::vector<int>> &cycles);
void print_cycles(const std::vector<std::vector<int>> &cycles);

#endif
